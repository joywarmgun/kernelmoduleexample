#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jesus Flores");
MODULE_DESCRIPTION("Mi primer modulo");
MODULE_VERSION("0.1");

static char *name = "mundo";	// Parametro del modulo
module_param(name, charp, S_IRUGO);
MODULE_PARM_DESC(name, "Nombre para mostrar en log");

int hello_init(void){
    printk(KERN_INFO "HMK: Hola %s\n", name);
    return 0;
}

void hello_cleanup(void){
    printk(KERN_INFO "HMK: Adios %s\n", name);
}

module_init(hello_init);
module_exit(hello_cleanup);
